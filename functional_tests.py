from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest, time, random, math

class NewVisitorTest(unittest.TestCase):
    

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        for i in range(10):
            self.browser.find_element_by_id("del_1").click()
        self.browser.quit()

    def test_web_page_coin_store(self):
        self.browser.get("localhost:8000")

        #Test title#
        self.assertEquals("Coins Store", self.browser.title)
        time.sleep(3)

        #Test insert item#
        num = [0 for i in range(10)]
        for i in range(10):
            num[i] = random.randint(0, 1000000)/100
            self.browser.find_element_by_id("money").send_keys(str(num[i]))
            self.browser.find_element_by_id("date").send_keys("2015-01-"+str(i+1).rjust(2, '0'))
            self.browser.find_element_by_id("save").click()
        time.sleep(3)

        #Test averages#
        total = 0.0
        for i in num:
            total += i
        avg = total/10
        rows = self.browser.find_element_by_id("datatable").find_elements_by_tag_name("tr")
        chkitems = rows[-1].text
        self.assertIn(str(avg), chkitems)

        #Test sort date old to new#
        rows = self.browser.find_element_by_id("datatable").find_elements_by_tag_name("tr")
        for i in range(10):
            self.assertTrue(any(str(num[i]) in row.text for row in rows))

        #Test sort date new to old#
        self.browser.find_element_by_id("new").click()
        self.browser.find_element_by_id("sort").click()
        rows = self.browser.find_element_by_id("datatable").find_elements_by_tag_name("tr")
        chkitems = [row.text for row in rows][1:-1:]
        for i in range(10)[::-1]:
            self.assertIn("2015-01-"+str(i+1).rjust(2, '0'), chkitems[::-1][i])

        #Test sort more to less#
        self.browser.find_element_by_id("more").click()
        self.browser.find_element_by_id("sort").click()
        num.sort()
        rows = self.browser.find_element_by_id("datatable").find_elements_by_tag_name("tr")
        chkitems = [row.text for row in rows][1:-1:]
        for i in range(10):
            self.assertIn(str(num[i]), chkitems[::-1][i])

        #Test sort less to more#
        self.browser.find_element_by_id("less").click()
        self.browser.find_element_by_id("sort").click()
        rows = self.browser.find_element_by_id("datatable").find_elements_by_tag_name("tr")
        chkitems = [row.text for row in rows][1:-1:]
        for i in range(10):
            self.assertIn(str(num[i]), chkitems[i])
        #Test finished#
        


if __name__ == '__main__':
    unittest.main(warnings='ignore')
