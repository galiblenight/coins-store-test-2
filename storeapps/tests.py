from django.test import TestCase
from storeapps.models import data
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from storeapps.views import home
import pep8


class modeltests(TestCase):

    def test_saving_and_retrieving_items(self):
        for i in range(10):
            items = data()
            items.money = i+1
            items.date = '2015-04-0'+str(i+1)
            items.dateint = int('2015040'+str(i+1))
            items.save()

        items = data.objects.all()
        self.assertEqual(items.count(), 10)

        for i in range(10):
            chkitems = items[i]
            self.assertEqual(chkitems.money, i+1)
            self.assertEqual(chkitems.dateint, int('2015040'+str(i+1)))
            self.assertEqual(chkitems.date, '2015-04-0'+str(i+1))


class home_page_and_views_tests(TestCase):

    def test_root_url_resolves_to_home_views(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_pep8_views(self):
        fchecker = pep8.Checker('storeapps/views.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings)" % file_errors)

    def test_pep8_models(self):
        fchecker = pep8.Checker('storeapps/models.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings)" % file_errors)

    def test_pep8_tests(self):
        fchecker = pep8.Checker('storeapps/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("Found %s errors (and warnings)" % file_errors)

    def test_home_html_return(self):
        request = HttpRequest()
        response = home(request)
        self.assertTrue(response.content.startswith(b'<html>'))
        self.assertIn(b'<title>Coins Store</title>', response.content)

    def test_home_views(self):
        request = HttpRequest()
        response = home(request)
        self.assertIn("<h2>No data in database please insert data</h2>",
                      response.content.decode())
        # Test request 1
        request = HttpRequest()
        request.method = "POST"
        request.POST["accept"] = "save"
        request.POST["money"] = "10.0"
        request.POST["date"] = "2015-01-01"
        response = home(request)
        # Test request 2
        request = HttpRequest()
        request.method = "POST"
        request.POST["accept"] = "save"
        request.POST["money"] = "20.0"
        request.POST["date"] = "2015-01-02"
        response = home(request)
        self.assertIn("10.0", response.content.decode())
        self.assertIn("20.0", response.content.decode())
        self.assertIn("2015-01-01", response.content.decode())
        self.assertIn("2015-01-02", response.content.decode())
