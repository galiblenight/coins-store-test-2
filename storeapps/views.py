from django.shortcuts import render
from django.http import HttpResponse
from storeapps.models import data
from datetime import date


def home(request):
    today = date.today().isoformat()
    sortby = request.POST.get('sortby', '')
    if request.method == "POST":
        delete = request.POST.get('del', '')
        accept = request.POST.get('accept', '')
        if accept == 'delete':
            data.objects.get(id=int(delete)).delete()
        elif accept == 'save':
            day = (request.POST.get('date', '') == '')\
                   and today or request.POST.get('date', '')
            intday = int(day.replace("-", "").replace("/", ""))
            data.objects.create(date=day,
                                dateint=intday,
                                money=request.POST.get('money', 0))
    if sortby == "new":
        item = data.objects.all().order_by('-dateint')
    elif sortby == "less":
        item = data.objects.all().order_by('money')
    elif sortby == "more":
        item = data.objects.all().order_by('-money')
    else:
        item = data.objects.all().order_by('dateint')
    total = 0
    for i in item:
        total += i.money
    d = ((data.objects.count() == 0) and 1 or data.objects.count())
    averages = total/d
    return render(request, 'home.html', {'data': item,
                                         'total': total,
                                         'averages': averages,
                                         'countitem': item.count(),
                                         'sort': sortby})
